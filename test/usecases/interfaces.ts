export interface Usecase
{
	contract: ContractFixture;
	cancellation: CancellationFixture;
	liquidation: LiquidationFixture;
	maturation: MaturationFixture;
}

export interface ContractFixture
{
	register: MethodFixture;
	create: MethodFixture;
	validate: MethodFixture;
}

export interface CancellationFixture
{
	findContractCoins: MethodFixture;
	abort: MethodFixture;
	cancel: MethodFixture;
}

export interface LiquidationFixture
{
	findContractCoins: MethodFixture;
	simulate: MethodFixture;
	liquidate: MethodFixture;
	payout: MethodFixture;
}

export interface MaturationFixture
{
	findContractCoins: MethodFixture;
	simulate: MethodFixture;
	mature: MethodFixture;
	payout: MethodFixture;
}

export interface MethodFixture
{
	input: any[];
	output: any;
}
