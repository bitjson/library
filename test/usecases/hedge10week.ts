import { Usecase } from './interfaces';

const usecase: Usecase =
{
	/*
	The data in this usecase example represents a 1-week long hedging contract of $10.
	The data was derived from a functional example with the following on-chain activity:
	NOTE: The data was updated manually as part of an oracle update so address and oracle message has since changed.

	CONTRACT
		https://blockchair.com/bitcoin-cash/address/bitcoincash:pqvfecpwxvj53ayqfwkxtjaxsgpvnklcyg8xewk9hl

	CANCELLATION
		https://blockchair.com/bitcoin-cash/transaction/4db095f34d632a4daf942142c291f1f2abb5ba2e1ccac919d85bdc2f671fb251
		https://blockchair.com/bitcoin-cash/transaction/2d992ef98bbf0f8e2996492ea90ca5479bc2d0ef8aab8efea226ec246ad82361

	LIQUIDATION
		https://blockchair.com/bitcoin-cash/transaction/8a80421d0683bf83720e07cb4f8ad64d69ad14a9020c67111eea886448f1c0cb
		https://blockchair.com/bitcoin-cash/transaction/b04f7300e316af7be3afd352495edef51bed492b94d5a8b9f2b9d50bf7801146

	MATURATION
		https://blockchair.com/bitcoin-cash/transaction/89021f0b795d79c8c6c48b14d4e7e86e2d55108389323e4d9d8c32a9a716b781
		https://blockchair.com/bitcoin-cash/transaction/c6a1d0e413ee8cecd0afeac908a767c527069784833cd6b1bd4d2c701ab6848b
	*/

	contract:
	{
		register:
		{
			input:
			[
				Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
				Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
				Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
				10,
				236,
				615643,
				0,
				1009,
				10,
				0.75,
			],
			output:
			{
				contract:
				{
					id: 1,
					address: 'bitcoincash:pp6ahpyqnv60uagx8r49cvslr7h6l4rpss7hvd4hyd',
				},
				parameters:
				{
					priceRangeMax: 2359,
					priceRangeMin: 178,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.from(' ', 'hex'),
					highLowDeltaTruncatedZeroes: Buffer.from(' ', 'hex'),
					hedgeUnitsXSatsPerBchHighTrunc: 999999968,
					payoutSatsLowTrunc: 5617978,
				},
				metadata:
				{
					oraclePublicKey: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgePublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					contractBlockHeight: 615643,
					maturityModifier: 1009,
					falloutModifier: 0,
					valueLimitUp: 10,
					valueLimitDown: 0.75,
					contractPrice: 236,
					hedgeAmount: 10,
					totalInputSats: 5617978,
					hedgeInputSats: 4237288,
					longInputSats: 1380690,
					dustCost: 1092,
					minerCost: 1500,
					longAmount: 3.2584284,
				},
				hashes:
				{
					mutualRedemptionDataHash: Buffer.from('3146a17a9555f4be5030895821f37494b9b4f7ad', 'hex'),
					payoutDataHash: Buffer.from('73a70da4f60f5ccc2b92308ac83bc27654af5b1c', 'hex'),
				},
			},
		},
		create:
		{
			input:
			[
				Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
				Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
				Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
				10,
				236,
				615643,
				0,
				1009,
				10,
				0.75,
			],
			output:
			{
				parameters:
				{
					priceRangeMax: 2359,
					priceRangeMin: 178,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.from(' ', 'hex'),
					highLowDeltaTruncatedZeroes: Buffer.from(' ', 'hex'),
					hedgeUnitsXSatsPerBchHighTrunc: 999999968,
					payoutSatsLowTrunc: 5617978,
				},
				metadata:
				{
					oraclePublicKey: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgePublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					contractBlockHeight: 615643,
					maturityModifier: 1009,
					falloutModifier: 0,
					valueLimitUp: 10,
					valueLimitDown: 0.75,
					contractPrice: 236,
					hedgeAmount: 10,
					totalInputSats: 5617978,
					hedgeInputSats: 4237288,
					longInputSats: 1380690,
					dustCost: 1092,
					minerCost: 1500,
					longAmount: 3.2584284,
				},
				hashes:
				{
					mutualRedemptionDataHash: Buffer.from('3146a17a9555f4be5030895821f37494b9b4f7ad', 'hex'),
					payoutDataHash: Buffer.from('73a70da4f60f5ccc2b92308ac83bc27654af5b1c', 'hex'),
				},
			},
		},
		validate:
		{
			input:
			[
				'bitcoincash:pp6ahpyqnv60uagx8r49cvslr7h6l4rpss7hvd4hyd',
				Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
				Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
				Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
				10,
				236,
				615643,
				0,
				1009,
				10,
				0.75,
			],
			output: true,
		},
	},
	cancellation:
	{
		findContractCoins:
		{
			input: [],
			output:
			{
				utxo:
				{
					txid: '4db095f34d632a4daf942142c291f1f2abb5ba2e1ccac919d85bdc2f671fb251',
					vout: 0,
					amount: 0.05620570,
					satoshis: 5620570,
					confirmations: 0,
					ts: 1578319894,
				},
			},
		},
		abort:
		{
			input:
			[
				'KwJQujgmyAzYXHLCzHmVT9YNorQ1tmbFb5xmZsNGMcom5EcubKHW',
				'L5QR5nGNSybEqYr5P8ikiuUnfrbDgA8jSsmxxxdzKohfRqr7ebti',
				{
					oraclePublicKey: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgePublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					contractBlockHeight: 615643,
					maturityModifier: 1009,
					falloutModifier: 0,
					valueLimitUp: 10,
					valueLimitDown: 0.75,
					contractPrice: 236,
					hedgeAmount: 10,
					totalInputSats: 5617978,
					hedgeInputSats: 4237288,
					longInputSats: 1380690,
					dustCost: 1092,
					minerCost: 1500,
					longAmount: 3.2584284,
				},
				{
					mutualRedemptionDataHash: Buffer.from('3146a17a9555f4be5030895821f37494b9b4f7ad', 'hex'),
					payoutDataHash: Buffer.from('73a70da4f60f5ccc2b92308ac83bc27654af5b1c', 'hex'),
				},
			],
			output: '2d992ef98bbf0f8e2996492ea90ca5479bc2d0ef8aab8efea226ec246ad82361',
		},
		cancel:
		{
			input:
			[
				'KwJQujgmyAzYXHLCzHmVT9YNorQ1tmbFb5xmZsNGMcom5EcubKHW',
				'L5QR5nGNSybEqYr5P8ikiuUnfrbDgA8jSsmxxxdzKohfRqr7ebti',
				4237288,
				1380690,
				{
					mutualRedemptionDataHash: Buffer.from('3146a17a9555f4be5030895821f37494b9b4f7ad', 'hex'),
					payoutDataHash: Buffer.from('73a70da4f60f5ccc2b92308ac83bc27654af5b1c', 'hex'),
				},
			],
			output: '2d992ef98bbf0f8e2996492ea90ca5479bc2d0ef8aab8efea226ec246ad82361',
		},
	},
	liquidation:
	{
		findContractCoins:
		{
			input: [],
			output:
			{
				utxo:
				{
					txid: '8a80421d0683bf83720e07cb4f8ad64d69ad14a9020c67111eea886448f1c0cb',
					vout: 0,
					amount: 0.05620570,
					satoshis: 5620570,
					confirmations: 0,
					ts: 1578319894,
				},
			},
		},
		simulate:
		{
			input:
			[
				{
					priceRangeMax: 2359,
					priceRangeMin: 178,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.from(' ', 'hex'),
					highLowDeltaTruncatedZeroes: Buffer.from(' ', 'hex'),
					hedgeUnitsXSatsPerBchHighTrunc: 999999968,
					payoutSatsLowTrunc: 5617978,
				},
				5620570,
				175,
			],
			output:
			{
				hedgePayoutSats: 5618491,
				longPayoutSats: 547,
				payoutSats: 5619038,
				minerFeeSats: 1532,
			},
		},
		liquidate:
		{
			input:
			[
				Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
				Buffer.from('af000000cb6809000000000011111111222222223333333344444444555555556666666677777777e8fd123456786257135e', 'hex'),
				Buffer.from('cde60262a4a9aea7e98731e60917a8de63108c4011e45a6c0fc7148348cf95c55d9fcf9e02b27e56ad3c3c84307e20c079a8036d3b1835193bf1f6485cbf30a1', 'hex'),
				Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
				Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
				{
					oraclePublicKey: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgePublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					contractBlockHeight: 615643,
					maturityModifier: 1009,
					falloutModifier: 0,
					valueLimitUp: 10,
					valueLimitDown: 0.75,
					contractPrice: 236,
					hedgeAmount: 10,
					totalInputSats: 5617978,
					hedgeInputSats: 4237288,
					longInputSats: 1380690,
					dustCost: 1092,
					minerCost: 1500,
					longAmount: 3.2584284,
				},
				{
					priceRangeMax: 2359,
					priceRangeMin: 178,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.from(' ', 'hex'),
					highLowDeltaTruncatedZeroes: Buffer.from(' ', 'hex'),
					hedgeUnitsXSatsPerBchHighTrunc: 999999968,
					payoutSatsLowTrunc: 5617978,
				},
				{
					mutualRedemptionDataHash: Buffer.from('3146a17a9555f4be5030895821f37494b9b4f7ad', 'hex'),
					payoutDataHash: Buffer.from('73a70da4f60f5ccc2b92308ac83bc27654af5b1c', 'hex'),
				},
			],
			output: 'b04f7300e316af7be3afd352495edef51bed492b94d5a8b9f2b9d50bf7801146',
		},
		payout:
		{
			input:
			[
				Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
				Buffer.from('af000000cb6809000000000011111111222222223333333344444444555555556666666677777777e8fd123456786257135e', 'hex'),
				Buffer.from('cde60262a4a9aea7e98731e60917a8de63108c4011e45a6c0fc7148348cf95c55d9fcf9e02b27e56ad3c3c84307e20c079a8036d3b1835193bf1f6485cbf30a1', 'hex'),
				Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
				Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
				5618491,
				547,
				{
					priceRangeMax: 2359,
					priceRangeMin: 178,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.from(' ', 'hex'),
					highLowDeltaTruncatedZeroes: Buffer.from(' ', 'hex'),
					hedgeUnitsXSatsPerBchHighTrunc: 999999968,
					payoutSatsLowTrunc: 5617978,
				},
				{
					mutualRedemptionDataHash: Buffer.from('3146a17a9555f4be5030895821f37494b9b4f7ad', 'hex'),
					payoutDataHash: Buffer.from('73a70da4f60f5ccc2b92308ac83bc27654af5b1c', 'hex'),
				},
			],
			output: 'b04f7300e316af7be3afd352495edef51bed492b94d5a8b9f2b9d50bf7801146',
		},
	},
	maturation:
	{
		findContractCoins:
		{
			input: [],
			output:
			{
				utxo:
				{
					txid: '89021f0b795d79c8c6c48b14d4e7e86e2d55108389323e4d9d8c32a9a716b781',
					vout: 0,
					amount: 0.05620570,
					satoshis: 5620570,
					confirmations: 0,
					ts: 1578319894,
				},
			},
		},
		simulate:
		{
			input:
			[
				{
					priceRangeMax: 2359,
					priceRangeMin: 178,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.from(' ', 'hex'),
					highLowDeltaTruncatedZeroes: Buffer.from(' ', 'hex'),
					hedgeUnitsXSatsPerBchHighTrunc: 999999968,
					payoutSatsLowTrunc: 5617978,
				},
				5620570,
				235,
			],
			output:
			{
				hedgePayoutSats: 4255351,
				longPayoutSats: 1362659,
				payoutSats: 5618010,
				minerFeeSats: 2560,
			},
		},
		mature:
		{
			input:
			[
				Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
				Buffer.from('eb000000cc68090000000000111111112222222233333333444444445555555566666666777777770100123456786d5c135e', 'hex'),
				Buffer.from('54b79e40a417724437c5e3fec7b27827efc79d2e3f8d5b14123787838beaa805c913a449c3510ec9bd623123be0e2cfbfce0b7af6326da0de2e817b949d4160a', 'hex'),
				Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
				Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
				{
					oraclePublicKey: '03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3',
					hedgePublicKey: '020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309',
					longPublicKey: '028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954',
					contractBlockHeight: 615643,
					maturityModifier: 1009,
					falloutModifier: 0,
					valueLimitUp: 10,
					valueLimitDown: 0.75,
					contractPrice: 236,
					hedgeAmount: 10,
					totalInputSats: 5617978,
					hedgeInputSats: 4237288,
					longInputSats: 1380690,
					dustCost: 1092,
					minerCost: 1500,
					longAmount: 3.2584284,
				},
				{
					priceRangeMax: 2359,
					priceRangeMin: 178,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.from(' ', 'hex'),
					highLowDeltaTruncatedZeroes: Buffer.from(' ', 'hex'),
					hedgeUnitsXSatsPerBchHighTrunc: 999999968,
					payoutSatsLowTrunc: 5617978,
				},
				{
					mutualRedemptionDataHash: Buffer.from('3146a17a9555f4be5030895821f37494b9b4f7ad', 'hex'),
					payoutDataHash: Buffer.from('73a70da4f60f5ccc2b92308ac83bc27654af5b1c', 'hex'),
				},
			],
			output: 'c6a1d0e413ee8cecd0afeac908a767c527069784833cd6b1bd4d2c701ab6848b',
		},
		payout:
		{
			input:
			[
				Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
				Buffer.from('eb000000cc68090000000000111111112222222233333333444444445555555566666666777777770100123456786d5c135e', 'hex'),
				Buffer.from('54b79e40a417724437c5e3fec7b27827efc79d2e3f8d5b14123787838beaa805c913a449c3510ec9bd623123be0e2cfbfce0b7af6326da0de2e817b949d4160a', 'hex'),
				Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
				Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
				4255351,
				1362659,
				{
					priceRangeMax: 2359,
					priceRangeMin: 178,
					earliestLiquidationHeight: 615643,
					maturityHeight: 616652,
					oraclePubk: Buffer.from('03677dfd72638a3ef385682cbdd46efb2e3264a244c12a3bd9527b7e08a97c17f3', 'hex'),
					hedgeLockScript: Buffer.from('1976a914285bb350881b21ac89724c6fb6dc914d096cd53b88ac', 'hex'),
					longLockScript: Buffer.from('1976a91445f1f1c4a9b9419a5088a3e9c24a293d7a150e6488ac', 'hex'),
					hedgeMutualRedeemPubk: Buffer.from('020797d8fd4d2fa6fd7cdeabe2526bfea2b90525d6e8ad506ec4ee3c53885aa309', 'hex'),
					longMutualRedeemPubk: Buffer.from('028a53f95eb631b460854fc836b2e5d31cad16364b4dc3d970babfbdcc3f2e4954', 'hex'),
					lowTruncatedZeroes: Buffer.from(' ', 'hex'),
					highLowDeltaTruncatedZeroes: Buffer.from(' ', 'hex'),
					hedgeUnitsXSatsPerBchHighTrunc: 999999968,
					payoutSatsLowTrunc: 5617978,
				},
				{
					mutualRedemptionDataHash: Buffer.from('3146a17a9555f4be5030895821f37494b9b4f7ad', 'hex'),
					payoutDataHash: Buffer.from('73a70da4f60f5ccc2b92308ac83bc27654af5b1c', 'hex'),
				},
			],
			output: 'c6a1d0e413ee8cecd0afeac908a767c527069784833cd6b1bd4d2c701ab6848b',
		},
	},
};

export = usecase;
