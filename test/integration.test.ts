import test, { ExecutionContext } from 'ava';
import { Usecase, usecases } from './usecases';
import AnyHedgeContract from '../lib/anyhedge';

const sinon = require('sinon');

// Create an instance of the contract manager.
const contract = new AnyHedgeContract();

// Declare usecase as a global-scope reference variable.
let usecase: Usecase;

// Set up contract creation test.
const testContractCreation = async function(t: ExecutionContext): Promise<void>
{
	// Create new contract.
	// @ts-ignore
	const contractData = await contract.create(...usecase.contract.create.input);

	// Build the contract.
	const contractInstance = await contract.build(contractData.hashes);

	// Derive the contract address.
	const contractAddress = contractInstance.address;

	// Verify that the contract data matches expections.
	t.deepEqual(contractAddress, usecase.contract.register.output.contract.address);
};

// Set up contract liquidation test.
const testContractLiquidation = async function(t: ExecutionContext): Promise<void>
{
	// Stub the broadcast function.
	const broadcastStub = sinon.stub(contract, 'broadcast');

	// Set the broadcast function to always return a given transaction ID.
	broadcastStub.returns(usecase.liquidation.payout.output);

	// Stub the find contract coins function.
	const findContractCoinsStub = sinon.stub(contract, 'findContractCoins');

	// Set the find coins to always return a static coin list.
	findContractCoinsStub.returns([ usecase.liquidation.findContractCoins.output ]);

	// Call the liquidate function.
	// @ts-ignore
	const liquidationResult = await contract.liquidate(...usecase.liquidation.liquidate.input);

	// Verify that the liquidation created the expected data.
	t.deepEqual(liquidationResult, usecase.liquidation.liquidate.output);

	// Restore the broadcast and find coin stub to prevent cascading errors.
	broadcastStub.restore();
	findContractCoinsStub.restore();
};

// Set up the contract maturation test.
const testContractMaturation = async function(t: ExecutionContext): Promise<void>
{
	// Stub the broadcast function.
	const broadcastStub = sinon.stub(contract, 'broadcast');

	// Set the broadcast function to always return a given transaction ID.
	broadcastStub.returns(usecase.maturation.payout.output);

	// Stub the find contract coins function.
	const findContractCoinsStub = sinon.stub(contract, 'findContractCoins');

	// Set the find coins to always return a static coin list.
	findContractCoinsStub.returns([ usecase.maturation.findContractCoins.output ]);

	// Call the mature function
	// @ts-ignore
	const maturationResult = await contract.mature(...usecase.maturation.mature.input);

	// Verify that the maturation created the expected data.
	t.deepEqual(maturationResult, usecase.maturation.mature.output);

	// Restore the broadcast and find coin stub to prevent cascading errors.
	broadcastStub.restore();
	findContractCoinsStub.restore();
};

// Set up the contract abortion test.
const testContractAbortion = async function(t: ExecutionContext): Promise<void>
{
	// Stub the broadcast function.
	const broadcastStub = sinon.stub(contract, 'broadcast');

	// Set the broadcast function to always return a given transaction ID.
	broadcastStub.returns(usecase.cancellation.cancel.output);

	// Stub the find contract coins function.
	const findContractCoinsStub = sinon.stub(contract, 'findContractCoins');

	// Set the find coins to always return a static coin list.
	findContractCoinsStub.returns([ usecase.cancellation.findContractCoins.output ]);

	// Call the abort function.
	// @ts-ignore
	const abortionResult = await contract.abort(...usecase.cancellation.abort.input);

	// Verify that the maturation created the expected data.
	t.deepEqual(abortionResult, usecase.cancellation.abort.output);

	// Restore the broadcast and find coin stub to prevent cascading errors.
	broadcastStub.restore();
	findContractCoinsStub.restore();
};

// Set up normal tests.
const runNormalTests = async function(): Promise<void>
{
	// For each usecase to test..
	for(const currentUsecase in usecases)
	{
		// .. assign it to the usecase global reference.
		usecase = usecases[currentUsecase];

		// Test top-level non-stubbed library functions in parallell with the current usecase.
		test('Create a contract', testContractCreation);

		// Test top-level stubbed library functions in series with the current usecase.
		test.serial('Liquidate a contract', testContractLiquidation);
		test.serial('Mature a contract', testContractMaturation);
		test.serial('Abort a contract', testContractAbortion);
	}
};

/*
// Define invalid test cases.
const runFailureTests = async function()
{
	// Process the verification of the example message and an empty signature.
	//let signatureStatus = await oracle.verifyMessage(example.message, null);

	// Verify that the signature verification results in false.
	//test.false(signatureStatus, 'Message verification should fail when the signature is empty.');

};
*/

const runTests = async function(): Promise<void>
{
	// Load the contract file.
	await contract.load();

	// Run normal and failure tests.
	await runNormalTests();
	// await runFailureTests();
};

// Run all tests.
runTests();
