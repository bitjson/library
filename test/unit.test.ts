import test, { ExecutionContext } from 'ava';
import { Usecase, usecases } from './usecases';
import AnyHedgeContract from '../lib/anyhedge';

// const sinon = require('sinon');

// Create an instance of the contract manager.
const contract = new AnyHedgeContract();

// Declare usecase as a global-scope reference variable.
let usecase: Usecase;

const testContractCreate = async function(t: ExecutionContext): Promise<void>
{
	// Create a new contract.
	// @ts-ignore
	const contractData = await contract.create(...usecase.contract.create.input);

	// Verify that the contract data matches expectations.
	t.deepEqual(contractData, usecase.contract.create.output);
};

const testContractValidate = async function(t: ExecutionContext): Promise<void>
{
	// Validate contract address.
	// @ts-ignore
	const contractValidity = await contract.validate(...usecase.contract.validate.input);

	// Verify that the contract is valid.
	t.deepEqual(contractValidity, usecase.contract.validate.output);
};

const testContractSimulateLiquidation = async function(t: ExecutionContext): Promise<void>
{
	// Simulate contract outcome.
	// @ts-ignore
	const simulationResults = await contract.simulate(...usecase.liquidation.simulate.input);

	// Verify that the simulation results matches expections.
	t.deepEqual(simulationResults, usecase.liquidation.simulate.output);
};

const testContractSimulateMaturation = async function(t: ExecutionContext): Promise<void>
{
	// Simulate contract outcome.
	// @ts-ignore
	const simulationResults = await contract.simulate(...usecase.maturation.simulate.input);

	// Verify that the simulation results matches expections.
	t.deepEqual(simulationResults, usecase.maturation.simulate.output);
};

// Set up normal tests.
const runNormalTests = async function(): Promise<void>
{
	// For each usecase to test..
	for(const currentUsecase in usecases)
	{
		// .. assign it to the usecase global reference.
		usecase = usecases[currentUsecase];

		// Test top-level non-stubbed library functions in parallell with the current usecase.
		test('Create a contract', testContractCreate);
		test('Validate a contract', testContractValidate);
		test('Simulate a contract liquidation', testContractSimulateLiquidation);
		test('Simulate a contract maturation', testContractSimulateMaturation);

		// Test top-level stubbed library functions in series with the current usecase.
		// NOTE: We currently don't have any stubbed library functions.
	}
};

/*
// Define invalid test cases.
const runFailureTests = async function()
{
	// Process the verification of the example message and an empty signature.
	//let signatureStatus = await oracle.verifyMessage(example.message, null);

	// Verify that the signature verification results in false.
	//test.false(signatureStatus, 'Message verification should fail when the signature is empty.');

};
*/
const runTests = async function(): Promise<void>
{
	// Load the contract file.
	await contract.load();

	// Run normal and failure tests.
	await runNormalTests();
	// await runFailureTests();
};

runTests();
