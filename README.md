# AnyHedge Library

Library for creation, validation and management of AnyHedge contracts.


## Installation

Install the library via NPM:

```
# npm install @generalprotocols/anyhedge
```

## Usage

### Setting up

Include the library into your project:

```js
// Load the AnyHedge library.
const anyHedgeLibrary = require('@generalprotocols/anyhedge');

// Create an instance of the contract manager.
const anyHedgeManager = new anyHedgeLibrary();
```

### Using provided functions

Creating a local contract:

```js
// Create new contract.
let contractData = await anyHedgeManager.create(...parameters);

// Build a contract instance.
let contractInstance = await anyHedgeManager.build(contractData.hashes);

// Derive the contract address.
let contractAddress = contractInstance.getAddress('mainnet');
```

Registering a contract with a redemption service:

```js
// Submit a contract for external redemption management.
let contractData = await anyHedgeManager.register(...parameters);
```

Validate that a contract matches a specific parameterization:

```js
// Validate a contract address.
let contractValidity = await anyHedgeManager.validate(...parameters);
```

**[WIP]** Fund a contract:

```js
// STEPS:
// 1. Determine who the parties to the contract will be.
// 2. Determine what unspent coins the parties will use.
// 3. Craft a transaction with following inputs covered:
//    - contractData.metadata.hedgeInputSats
//    - contractData.metadata.longInputSats
//    - contractData.metadata.dustCost
//    - contractData.metadata.minerCost
// 4. Pass the transaction to each party for signing.
// 5. Broadcast the funding transaction.
```

Liquidate, mature or cancel a contract:

```js
// Liquidate a contract.
let liquidationResult = await anyHedgeManager.liquidate(...parameters);

// Mature a contract.
let maturationResult = await anyHedgeManager.mature(...parameters);

// Cancel a contract to pay out current valuation.
let settlementResult = await contract.settle(...parameters);

// Cancel a contract to pay back initial funding.
let abortResult = await contract.abort(...parameters);
```
