import { Crypto } from 'bitbox-sdk';
import { debug, range } from './javascript-util';
import { SCRIPT_INT_MAX, MAX_TRUNC_BYTES } from '../constants';

/**
* Helper function to bitshift down with truncation.
*
* @param data    a number to bitshift and truncate.
* @param bytes   how many bytes to bitshift.
*
* @returns the bitshifted and then truncated number.
*/
export const truncScriptNum = function(data: number, bytes: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'trunc() <=', arguments ]);

	// Bit-shift the data by proxy.
	const result = Math.floor(data / (2 ** (8 * bytes)));

	// Output function result for easier collection of test data.
	debug.result([ 'trunc() =>', result ]);

	// Return the variable buffer encoded data.
	return result;
};

/**
* Helper function to bitshift up with truncation.
*
* @param data    a number to bitshift and truncate.
* @param bytes   how many bytes to bitshift.
*
* @returns the bitshifted and then truncated number.
*/
export const untruncScriptNum = function(data: number, bytes: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'untrunc() <=', arguments ]);

	// Bit-shift the data by proxy.
	const result = Math.floor(data * (2 ** (8 * bytes)));

	// Output function result for easier collection of test data.
	debug.result([ 'untrunc() =>', result ]);

	// Return the variable buffer encoded data.
	return result;
};

/**
* Helper function to determine how many bytes we need to shift to fit a number
* within the bitcoin cash script limitations.
*
* @param value   a number to determine bitshift requirements for.
*
* @returns an integer stating how many bytes we need to shift the value.
*/
export const calculateRequiredScriptNumTruncation = function(value: number): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'calculateTruncationSize() <=', arguments ]);

	let truncationLevel;
	for(const truncationSize in range(MAX_TRUNC_BYTES + 1))
	{
		// Truncate the value at this size.
		const truncatedValue = truncScriptNum(value, Number(truncationSize));

		// Check if this truncation size is sufficient..
		if(truncatedValue >= 1 && truncatedValue <= SCRIPT_INT_MAX)
		{
			// Store the current size as a suitable truncatin level.
			truncationLevel = Number(truncationSize);

			// Stop looking now that we have a suitable truncation level.
			break;
		}
	}

	if(truncationLevel === undefined)
	{
		// Since no truncation size was matched, throw an error.
		throw(new Error(`Failed to find a suitable truncation level for '${value}'`));
	}

	// Output function result for easier collection of test data.
	debug.result([ 'calculateTruncationSize() =>', truncationLevel ]);

	// return the matching truncation size.
	return truncationLevel;
};

/**
* Helper function to construct a P2PKH locking script Buffer from a public key
*
* @param publicKey   Public key to create a P2PKH locking script for
*
* @returns a P2PKH locking script corresponding to the passed public key
*/
export const buildLockScriptP2PKH = function(publicKey: Buffer): Buffer
{
	const pkh = (new Crypto()).hash160(publicKey);

	// Build P2PKH lock script (PUSH<25> DUP HASH160 PUSH<20> <pkh> EQUALVERIFY CHECKSIG)
	return Buffer.concat([ Buffer.from('1976a914', 'hex'), pkh, Buffer.from('88ac', 'hex') ]);
};
