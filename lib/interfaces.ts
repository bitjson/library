export interface ContractData
{
	contract?: ContractRegistration;
	parameters: ContractParameters;
	metadata: ContractMetadata;
	hashes: ContractHashes;
}

export interface ContractRegistration
{
	id: number;
	address: string;
}

export interface ContractParameters
{
	priceRangeMin: number;
	priceRangeMax: number;
	earliestLiquidationHeight: number;
	maturityHeight: number;
	oraclePubk: Buffer;
	hedgeLockScript: Buffer;
	longLockScript: Buffer;
	hedgeMutualRedeemPubk: Buffer;
	longMutualRedeemPubk: Buffer;
	lowTruncatedZeroes: Buffer;
	highLowDeltaTruncatedZeroes: Buffer;
	hedgeUnitsXSatsPerBchHighTrunc: number;
	payoutSatsLowTrunc: number;
}

export interface ContractMetadata
{
	oraclePublicKey: string;
	hedgePublicKey: string;
	longPublicKey: string;
	contractBlockHeight: number;
	maturityModifier: number;
	falloutModifier: number;
	valueLimitUp: number;
	valueLimitDown: number;
	contractPrice: number;
	hedgeAmount: number;
	longAmount: number;
	totalInputSats: number;
	hedgeInputSats: number;
	longInputSats: number;
	dustCost: number;
	minerCost: number;
}

export interface ContractHashes
{
	mutualRedemptionDataHash: Buffer;
	payoutDataHash: Buffer;
}

export interface SimulationOutput
{
	hedgePayoutSats: number;
	longPayoutSats: number;
	payoutSats: number;
	minerFeeSats: number;
}
