; Compiled on 2020-05-27
<payoutDataHash> <mutualRedemptionDataHash>
; Mutual Redeem
OP_2 OP_PICK OP_0 OP_NUMEQUAL OP_IF
    ; Verify data hash
    OP_3 OP_PICK OP_5 OP_PICK OP_CAT OP_HASH160 OP_EQUALVERIFY
    ; Verify hedge sig
    OP_4 OP_ROLL OP_3 OP_ROLL OP_CHECKSIGVERIFY
    ; Verify long sig
    OP_2SWAP OP_CHECKSIG
    ; Cleanup
    OP_NIP OP_NIP
; Payout
OP_ELSE OP_ROT OP_1 OP_NUMEQUALVERIFY
    ; Decode preimage
    OP_2 OP_PICK OP_SIZE 28 OP_SUB OP_SPLIT OP_NIP 20 OP_SPLIT OP_DROP
    ; Verify data hash
    OP_ROT OP_4 OP_PICK OP_6 OP_PICK OP_CAT OP_7 OP_PICK OP_CAT OP_8 OP_PICK OP_CAT
    OP_9 OP_PICK OP_CAT OP_10 OP_PICK OP_CAT OP_11 OP_PICK OP_CAT OP_12 OP_PICK OP_CAT
    OP_13 OP_PICK OP_CAT OP_14 OP_PICK OP_CAT OP_15 OP_PICK OP_CAT
    OP_HASH160 OP_EQUALVERIFY
    ; Verify oracle sig
    11 OP_ROLL 11 OP_PICK OP_7 OP_ROLL OP_CHECKDATASIGVERIFY
    ; Verify preimage
    OP_13 OP_ROLL OP_14 OP_ROLL OP_2DUP OP_SWAP OP_SIZE OP_1SUB OP_SPLIT OP_DROP
    OP_6 OP_ROLL OP_SHA256 OP_ROT OP_CHECKDATASIGVERIFY OP_CHECKSIGVERIFY
    ; Decode oracle message
    OP_12 OP_PICK OP_4 OP_SPLIT OP_NIP
    OP_DUP 24 OP_SPLIT OP_NIP
    OP_2 OP_SPLIT OP_DROP OP_BIN2NUM
    OP_14 OP_ROLL OP_4 OP_SPLIT OP_DROP OP_BIN2NUM
    OP_ROT OP_4 OP_SPLIT OP_DROP OP_BIN2NUM
    ; Check oracle price
    OP_OVER OP_0 OP_GREATERTHAN OP_VERIFY
    ; Clamp price
    OP_OVER OP_13 OP_ROLL OP_MIN OP_12 OP_ROLL OP_MAX
    ; Check oracle height
    OP_OVER ff64cd1d OP_LESSTHAN OP_VERIFY
    ; Check locktime
    OP_OVER OP_CHECKLOCKTIMEVERIFY OP_DROP
    ; Check oracleHeight in bounds
    OP_OVER OP_13 OP_ROLL OP_14 OP_PICK OP_1ADD OP_WITHIN OP_VERIFY
    ; Check oracle price out of bounds
    OP_SWAP OP_12 OP_ROLL OP_LESSTHAN OP_IF
        OP_2DUP OP_NUMNOTEQUAL OP_VERIFY
    ; Check exactly at maturity height
    OP_ELSE
        OP_2 OP_PICK OP_1 OP_NUMEQUALVERIFY
    OP_ENDIF
    ; Truncation math
    OP_7 OP_PICK OP_OVER OP_DIV
    OP_9 OP_PICK OP_SWAP OP_4 OP_NUM2BIN OP_CAT OP_BIN2NUM
    OP_8 OP_ROLL OP_2 OP_PICK OP_MOD
    OP_9 OP_ROLL OP_SWAP OP_4 OP_NUM2BIN OP_CAT OP_BIN2NUM
    OP_ROT OP_DIV OP_ADD
    OP_7 OP_ROLL OP_OVER OP_SUB
    ; Verify hash outputs
    2202 OP_8 OP_NUM2BIN
    OP_9 OP_ROLL
    OP_8 OP_OVER OP_SIZE OP_NIP OP_SUB
    OP_OVER OP_5 OP_ROLL OP_2 OP_PICK OP_NUM2BIN OP_CAT OP_3 OP_PICK OP_OR
    OP_9 OP_ROLL OP_CAT
    OP_ROT OP_4 OP_ROLL OP_3 OP_ROLL OP_NUM2BIN OP_CAT OP_ROT OP_OR
    OP_6 OP_ROLL OP_CAT
    OP_4 OP_ROLL OP_ROT OP_ROT OP_CAT OP_HASH256 OP_EQUAL
    ; Cleanup
    OP_NIP OP_NIP OP_NIP
OP_ENDIF
