import { BITBOX } from 'bitbox-sdk';
import { Contract, SignatureTemplate, Transaction, Utxo, Instance } from 'cashscript';
import fetch from 'node-fetch';
import { DUST_LIMIT, SATS_PER_BCH, MIN_TRANSACTION_FEE } from './constants';
import { ContractHashes, ContractMetadata, ContractParameters, SimulationOutput, ContractData } from './interfaces';
import { debug, bufferReviver } from './util/javascript-util';
import { untruncScriptNum, calculateRequiredScriptNumTruncation, truncScriptNum, buildLockScriptP2PKH } from './util/anyhedge-util';

const PriceOracle = require('@generalprotocols/price-oracle');

// Initialize the BITBOX Bitcoin Cash library
const bitbox = new BITBOX();

export = class AnyHedgeContract
{
	contractFactory: Contract;

	/**
	* Loads a contract from file and prepares a contract factory.
	*
	* @param contractFile   Optional filepath for an alternative contract file to use instead of the default.
	*/
	async load(contractFile?: string): Promise<void>
	{
		try
		{
			// Use default contract if none was specified.
			const filename = (contractFile || require.resolve('./anyhedge.cash'));

			// Compile the foward contract.
			this.contractFactory = Contract.compile(filename, 'mainnet');
		}
		catch(error)
		{
			debug.errors('Failed to compile contract.');
			debug.object(error);
		}
	}

	/*
	// External library functions
	*/

	/**
	* Register a new contract for external management.
	*
	* @param oraclePublicKeyBuffer   public key for the oracle that the contract trusts for price messages.
	* @param hedgePublicKeyBuffer    public key for the hedge party.
	* @param longPublicKeyBuffer     public key for the long party.
	* @param hedgeAmount             amount in units that the hedge party wants to protect against volatility.
	* @param contractPrice           starting price (units/BCH) of the contract.
	* @param contractBlockHeight     blockHeight at which the contract is considered to have been started at.
	* @param falloutModifier         minimum number of blocks from the starting height before the contract can be liquidated.
	* @param maturityModifier        exact number of blocks from the starting height that the contract should mature at.
	* @param valueLimitUp            multiplier for the contractPrice determining the upper liquidation price boundary.
	* @param valueLimitDown          multiplier for the contractPrice determining the lower liquidation price boundary.
	* @param serviceDomain           fully qualified domain name for the settlement service provider to register with.
	* @param servicePort             network port number for the settlement service provider to register with.
	*
	* @returns contract information for the registered contract.
	*/
	async register(
		oraclePublicKeyBuffer: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		hedgeAmount: number,
		contractPrice: number,
		contractBlockHeight: number,
		falloutModifier: number,
		maturityModifier: number,
		valueLimitUp: number,
		valueLimitDown: number,
		serviceDomain = 'localhost',
		servicePort = 6572,
	): Promise<ContractData | undefined>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'register() <=', arguments ]);

		const contractParameters =
		{
			// Public keys
			oraclePublicKey: oraclePublicKeyBuffer.toString('hex'),
			hedgePublicKey: hedgePublicKeyBuffer.toString('hex'),
			longPublicKey: longPublicKeyBuffer.toString('hex'),

			// Contract parameters
			hedgeAmount: hedgeAmount,
			contractPrice: contractPrice,
			contractBlockHeight: contractBlockHeight,
			falloutModifier: falloutModifier,
			maturityModifier: maturityModifier,
			valueLimitUp: valueLimitUp,
			valueLimitDown: valueLimitDown,
		};

		// Register the contract with the automatic settlement service provider.
		const requestParameters =
		{
			method: 'post',
			body: JSON.stringify(contractParameters),
			headers: { 'Content-Type': 'application/json' },
		};
		const contractResponse = await fetch(`http://${serviceDomain}:${servicePort}/contract`, requestParameters);
		const contractData = await JSON.parse(await contractResponse.json(), bufferReviver);

		// Write log entry for easier debugging.
		debug.action(`Registered a newly created contract with ${serviceDomain}:${servicePort} for automatic settlement.`);

		// Validate that the contract returned is identical to a contract created locally.
		const contractValidity = await this.validate(
			contractData.contract.address,
			oraclePublicKeyBuffer,
			hedgePublicKeyBuffer,
			longPublicKeyBuffer,
			hedgeAmount,
			contractPrice,
			contractBlockHeight,
			falloutModifier,
			maturityModifier,
			valueLimitUp,
			valueLimitDown,
		);

		// If the contract is invalid..
		if(!contractValidity)
		{
			// Write a log entry explaining the problem and throw the error.
			const errorMsg = `Contract registration with ${serviceDomain}:${servicePort} resulted in an invalid contract.`;
			debug.errors(errorMsg);

			throw(new Error(errorMsg));
		}

		// Output function result for easier collection of test data.
		debug.result([ 'register() =>', contractData ]);

		// Return the contract information.
		return contractData;
	}

	/**
	* Validates that a given contract address matches specific contract parameters.
	*
	* @param contractAddress         contract address encoded according to the cashaddr specification.
	* @param oraclePublicKeyBuffer   public key for the oracle that the contract trusts for price messages.
	* @param hedgePublicKeyBuffer    public key for the hedge party.
	* @param longPublicKeyBuffer     public key for the long party.
	* @param hedgeAmount             amount in units that the hedge party wants to protect against volatility.
	* @param contractPrice           starting price (units/BCH) of the contract.
	* @param contractBlockHeight     blockHeight at which the contract is considered to have been started at.
	* @param falloutModifier         minimum number of blocks from the starting height before the contract can be liquidated.
	* @param maturityModifier        exact number of blocks from the starting height that the contract should mature at.
	* @param valueLimitUp            multiplier for the contractPrice determining the upper liquidation price boundary.
	* @param valueLimitDown          multiplier for the contractPrice determining the lower liquidation price boundary.
	*
	* @returns true if the contract address and parameters match, otherwise false.
	*/
	async validate(
		contractAddress: string,
		oraclePublicKeyBuffer: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		hedgeAmount: number,
		contractPrice: number,
		contractBlockHeight: number,
		falloutModifier: number,
		maturityModifier: number,
		valueLimitUp: number,
		valueLimitDown: number,
	): Promise<boolean>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'validate() <=', arguments ]);

		// Prepare the contract.
		const contractData = await this.create(
			oraclePublicKeyBuffer,
			hedgePublicKeyBuffer,
			longPublicKeyBuffer,
			hedgeAmount,
			contractPrice,
			contractBlockHeight,
			falloutModifier,
			maturityModifier,
			valueLimitUp,
			valueLimitDown,
		);

		// Build the contract.
		const contractInstance = await this.build(contractData.hashes);

		// Calculate contract validity.
		const contractValidity = (contractAddress === contractInstance.address);

		if(contractValidity)
		{
			// Write log entry for easier debugging.
			debug.action('Validated a contract address against provided contract parameters.');
			// eslint-disable-next-line prefer-rest-params
			debug.object(arguments);
		}
		else
		{
			// Write log entry for easier debugging.
			debug.errors('Failed to validate a contract address against provided contract parameters.');
			// eslint-disable-next-line prefer-rest-params
			debug.object(arguments);
		}

		// Output function result for easier collection of test data.
		debug.result([ 'validate() =>', contractValidity ]);

		// Return the validity of the contract.
		return contractValidity;
	}

	/**
	* Fetches the total balance on a contract.
	*
	* @param contractHashes   contract hashes required to build the contract instance.
	*
	* @returns the total number of unspent satoshis at the contracts address.
	*/
	async balance(contractHashes: ContractHashes): Promise<number>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'balance() <=', arguments ]);

		// Build the contract instance.
		const contractInstance = await this.build(contractHashes);

		const balance = await contractInstance.getBalance();

		// Write log entry for easier debugging.
		debug.action('Fetched available on-chain balance for contract.');

		// Output function result for easier collection of test data.
		debug.result([ 'balance() =>', balance ]);

		// Return the balance of the contract.
		return balance;
	}

	/**
	* Mutual cancellation based on original amounts.
	*
	* @param hedgePrivateKeyWIF   private key of the hedge party.
	* @param longPrivateKeyWIF    private key of the long party.
	* @param contractMetadata     contract metadata with hedge and longs input satoshis.
	* @param contractHashes       contract hashes required to build the contract instance.
	*
	* @returns the TXID of a successful cancellation.
	*/
	async abort(
		hedgePrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		contractMetadata: ContractMetadata,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'abort() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Attempting to abort a contract.');

		// Cancel the contract with the original amounts.
		const cancellationResult = await this.cancel(
			hedgePrivateKeyWIF,
			longPrivateKeyWIF,
			contractMetadata.hedgeInputSats,
			contractMetadata.longInputSats,
			contractHashes,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'abort() =>', cancellationResult ]);

		// Return the cancellation results.
		return cancellationResult;
	}

	/**
	* Mutual cancellation based on given valuation
	*
	* This is practically a mutually agreed upon maturation prior to the maturity height.
	*
	* @param hedgePrivateKeyWIF   private key of the hedge party.
	* @param longPrivateKeyWIF    private key of the long party.
	* @param settlementPrice      price (units/BCH) to use for the settlement.
	* @param contractParameters   contract parameters necessary to simulate maturaty outcome.
	* @param contractHashes       contract hashes required to build the contract instance.
	*
	* @returns the TXID of a successful cancellation.
	*/
	async settle(
		hedgePrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		settlementPrice: number,
		contractParameters: ContractParameters,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'settle() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Attempting to settle a contract.');

		// Fetch the current contract balance.
		const totalSats = await this.balance(contractHashes);

		// Calculate outcome amounts based on valuation.
		const outcome = await this.simulate(contractParameters, totalSats, settlementPrice);

		// Cancel the contract with the calculated outcome.
		const cancellationResult = await this.cancel(
			hedgePrivateKeyWIF,
			longPrivateKeyWIF,
			outcome.hedgePayoutSats,
			outcome.longPayoutSats,
			contractHashes,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'settle() =>', cancellationResult ]);

		// Return the settlement results.
		return cancellationResult;
	}

	/**
	* Mutual cancellation of a contract with arbitrary numbers.
	*
	* @param hedgePrivateKeyWIF   private key of the hedge party.
	* @param longPrivateKeyWIF    private key of the long party.
	* @param hedgePayoutSats      number of satoshis to pay to the hedge party.
	* @param longPayoutSats       number of satoshis to pay to the long party.
	* @param contractHashes       contract hashes required to build the contract instance.
	*
	* @returns the TXID of a successful cancellation.
	*/
	async cancel(
		hedgePrivateKeyWIF: string,
		longPrivateKeyWIF: string,
		hedgePayoutSats: number,
		longPayoutSats: number,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'cancel() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Cancelling a contract.');

		try
		{
			// Derive hedge and long keypairs.
			const hedgePrivateKeyPair = bitbox.ECPair.fromWIF(hedgePrivateKeyWIF);
			const longPrivateKeyPair = bitbox.ECPair.fromWIF(longPrivateKeyWIF);

			// Derive hedge and long public keys.
			const hedgePublicKeyBuffer = bitbox.ECPair.toPublicKey(hedgePrivateKeyPair);
			const longPublicKeyBuffer = bitbox.ECPair.toPublicKey(longPrivateKeyPair);

			// Create hedge and long signature templates
			const hedgeSigTemplate = new SignatureTemplate(hedgePrivateKeyPair);
			const longSigTemplate = new SignatureTemplate(longPrivateKeyPair);

			// Build the contract instance.
			const contractInstance = await this.build(contractHashes);

			// Fetch all UTXOs at the address.
			const coins = await this.findContractCoins(contractInstance);

			// Derive hedge and long address.
			const hedgeAddress = bitbox.ECPair.toCashAddress(hedgePrivateKeyPair);
			const longAddress = bitbox.ECPair.toCashAddress(longPrivateKeyPair);

			// Create a cancellation transaction..
			const cancellationTransaction = contractInstance.functions
				.mutualRedeem(hedgePublicKeyBuffer, longPublicKeyBuffer, hedgeSigTemplate, longSigTemplate)
				.from(coins)
				.withMinChange(Number.MAX_VALUE)
				.to(hedgeAddress, hedgePayoutSats)
				.to(longAddress, longPayoutSats);

			// Broadcast the transaction.
			const broadcastResult = await this.broadcast(cancellationTransaction);

			// Output function result for easier collection of test data.
			debug.result([ 'cancel() =>', broadcastResult ]);

			// Return the broadcast result.
			return broadcastResult;
		}
		catch(error)
		{
			// Log an error message.
			debug.errors('Failed to cancel contract: ', error);

			// Throw the error.
			throw(new Error(`Failed to cancel contract: ${JSON.stringify(error)}`));
		}
	}

	/**
	* Liquidates a contract.
	*
	* @param oraclePublicKey        public key of the oracle that provided the price message.
	* @param oracleMessage          price message to use for liquidation.
	* @param oracleSignature        signature for the price message.
	* @param hedgePublicKeyBuffer   public key of the hedge party.
	* @param longPublicKeyBuffer    public key of the long party.
	* @param contractMetadata       contract metadata required to determine available satoshis.
	* @param contractParameters     contract parameters required to simulate liquidation outcome.
	* @param contractHashes         contract hashes required to build the contract instance.
	*
	* @returns the TXID of a successful liquidation.
	*/
	async liquidate(
		oraclePublicKey: Buffer,
		oracleMessage: Buffer,
		oracleSignature: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		contractMetadata: ContractMetadata,
		contractParameters: ContractParameters,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'liquidate() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Attempting to liquidate a contract.');

		// Parse the oracle message.
		const oracleData = await PriceOracle.parseMessage(oracleMessage);

		// Validate that the oracles message blockheight is equal or higher than the contracts earliest liquidate height.
		if(oracleData.blockHeight < contractParameters.earliestLiquidationHeight)
		{
			// Log an error message.
			debug.errors('Cannot liquidate a contract before its earliest liquidation height.');

			// Throw error.
			throw(new Error('Cannot liquidate a contract before its earliest liquidation height.'));
		}

		// Validate that the oracles price is outside the contracts boundaries.
		if(oracleData.price >= contractParameters.priceRangeMin && oracleData.price <= contractParameters.priceRangeMax)
		{
			// Log an error message.
			debug.errors('Cannot liquidate a contract at a price within the contract boundaries.');

			// Throw the error.
			throw(new Error('Cannot liquidate a contract at a price within the contract boundaries.'));
		}

		// Calculate contract outcomes.
		const totalSats = contractMetadata.hedgeInputSats + contractMetadata.longInputSats + contractMetadata.minerCost + contractMetadata.dustCost;
		const outcome = await this.simulate(contractParameters, totalSats, oracleData.price);

		// Redeem the contract.
		const liquidationResult = await this.payout(
			oraclePublicKey,
			oracleMessage,
			oracleSignature,
			hedgePublicKeyBuffer,
			longPublicKeyBuffer,
			outcome.hedgePayoutSats,
			outcome.longPayoutSats,
			contractParameters,
			contractHashes,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'liquidate() =>', liquidationResult ]);

		// Return the liquidation result.
		return liquidationResult;
	}

	/**
	* Matures a contract.
	*
	* @param oraclePublicKey        public key of the oracle that provided the price message.
	* @param oracleMessage          price message to use for maturation.
	* @param oracleSignature        signature for the price message.
	* @param hedgePublicKeyBuffer   public key of the hedge party.
	* @param longPublicKeyBuffer    public key of the long party.
	* @param contractMetadata       contract metadata required to determine available satoshis.
	* @param contractParameters     contract parameters required to simulate maturation outcome.
	* @param contractHashes         contract hashes required to build the contract instance.
	*
	* @returns the TXID of a successful maturation.
	*/
	async mature(
		oraclePublicKey: Buffer,
		oracleMessage: Buffer,
		oracleSignature: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		contractMetadata: ContractMetadata,
		contractParameters: ContractParameters,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'mature() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Attempting to mature a contract.');

		// Parse the oracle message.
		const oracleData = await PriceOracle.parseMessage(oracleMessage);

		// Validate that the oracle messages blockheight is equal or higher than the contracts maturity height.
		if(oracleData.blockHeight < contractParameters.maturityHeight)
		{
			// Log an error message.
			debug.errors('Cannot mature a contract before its maturity height.');

			// Throw an error.
			throw(new Error('Cannot mature a contract before its maturity height.'));
		}

		// NOTE: We let the miners validate that the current blockheight is after the maturity height, to avoid network lookups.

		// Validate the that oracle messages block sequence number is 1.
		if(oracleData.blockSequence !== 1)
		{
			// Log an error message.
			debug.errors('Cannot mature a contract with an oracle block sequence other than 1.');

			// Throw an error.
			throw(new Error('Cannot mature a contract with an oracle block sequence other than 1.'));
		}

		// Calculate contract outcomes.
		const totalSats = contractMetadata.hedgeInputSats + contractMetadata.longInputSats + contractMetadata.minerCost + contractMetadata.dustCost;
		const outcome = await this.simulate(contractParameters, totalSats, oracleData.price);

		// Redeem the contract.
		const maturationResult = await this.payout(
			oraclePublicKey,
			oracleMessage,
			oracleSignature,
			hedgePublicKeyBuffer,
			longPublicKeyBuffer,
			outcome.hedgePayoutSats,
			outcome.longPayoutSats,
			contractParameters,
			contractHashes,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'mature() =>', maturationResult ]);

		// Return the liquidation result.
		return maturationResult;
	}

	/**
	* Redeems the contract with arbitrary numbers.
	*
	* @param oraclePublicKey        public key of the oracle that provided the price message.
	* @param oracleMessage          price message to use for payout.
	* @param oracleSignature        signature for the price message.
	* @param hedgePublicKeyBuffer   public key of the hedge party.
	* @param longPublicKeyBuffer    public key of the long party.
	* @param hedgePayoutSats        number of satoshis to pay out to the hedge party.
	* @param longPayoutSats         number of satoshis to pay out to the long party.
	* @param contractParameters     contract parameters required to unlock the redemption function.
	* @param contractHashes         contract hashes required to build the contract instance.
	*
	* @returns the TXID of a successful redemption.
	*/
	async payout(
		oraclePublicKey: Buffer,
		oracleMessage: Buffer,
		oracleSignature: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		hedgePayoutSats: number,
		longPayoutSats: number,
		contractParameters: ContractParameters,
		contractHashes: ContractHashes,
	): Promise<string>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'payout() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Paying out a contract.');

		try
		{
			// Build the contract instance.
			const contractInstance = await this.build(contractHashes);

			// Fetch the coins on the contract.
			const coins = await this.findContractCoins(contractInstance);

			// Generate a new random redeemwallet.
			const redeemWallet = bitbox.HDNode.fromSeed(bitbox.Mnemonic.toSeed(bitbox.Mnemonic.generate(128)));

			// Store the redeemwallets public key as the preimage pubkey.
			const preimagePubkey = redeemWallet.getPublicKeyBuffer();
			const preimageSigTemplate = new SignatureTemplate(redeemWallet.keyPair);

			// Derive the hedge and long address.
			const hedgePublicAddress = bitbox.ECPair.toCashAddress(bitbox.ECPair.fromPublicKey(hedgePublicKeyBuffer));
			const longPublicAddress = bitbox.ECPair.toCashAddress(bitbox.ECPair.fromPublicKey(longPublicKeyBuffer));

			// Create the transaction.
			// NOTE: CashScript automatically sets nSequence to allow for absolute locktimes
			// and sets nLocktime to the most recent block number
			const payoutTransaction = contractInstance.functions
				.payout(
					// // FIXED // //
					// Outputs
					contractParameters.hedgeLockScript,
					contractParameters.longLockScript,
					// Oracle
					contractParameters.oraclePubk,
					// Money
					contractParameters.hedgeUnitsXSatsPerBchHighTrunc,
					contractParameters.highLowDeltaTruncatedZeroes,
					contractParameters.payoutSatsLowTrunc,
					contractParameters.lowTruncatedZeroes,
					contractParameters.priceRangeMin,
					contractParameters.priceRangeMax,
					// Time
					contractParameters.earliestLiquidationHeight,
					contractParameters.maturityHeight,
					// // REDEMPTION // //
					// Transaction verification
					preimageSigTemplate,
					preimagePubkey,
					// Oracle data
					oracleMessage,
					oracleSignature,
				)
				.from(coins)
				.withMinChange(Number.MAX_VALUE)
				.to(hedgePublicAddress, hedgePayoutSats)
				.to(longPublicAddress, longPayoutSats);

			// Broadcast the transaction.
			const broadcastResult = await this.broadcast(payoutTransaction);

			// Output function result for easier collection of test data.
			debug.result([ 'payout() =>', broadcastResult ]);

			// Return the broadcast result.
			return broadcastResult;
		}
		catch(error)
		{
			// Log an error message.
			debug.errors('Failed to payout contract: ', error);

			// Throw the error.
			throw(new Error(`Failed to payout contract: ${JSON.stringify(error)}`));
		}
	}

	/*
	// Internal library functions
	*/

	/**
	* Wrapper that broadcasts a prepared transaction using the CashScript SDK.
	*
	* @param transactionBuilder   fully prepared transaction builder ready to execute its broadcast() function.
	*
	* @returns the TXID of a successful transaction.
	*/
	async broadcast(transactionBuilder: Transaction): Promise<string>
	{
		try
		{
			// Broadcast the signed transaction.
			const { txid } = await transactionBuilder.send();

			// Return the transaction ID.
			return txid;
		}
		catch(error)
		{
			// Build a raw signed transaction.
			const transactionHex = await transactionBuilder.build();

			// Log an error message.
			debug.errors('Failed to broadcast transaction: ', error);
			debug.errors(transactionHex);

			// Throw the error.
			throw error;
		}
	}

	/**
	* Wrapper that finds UTXOs for a contract
	*
	* @param contractInstance   fully prepared contract instance to call findCoins() on.
	*
	* @returns the list of UTXOs in the contract instance's control
	*/
	async findContractCoins(contractInstance: Instance): Promise<Utxo[]>
	{
		// Return a list of all UTXOs at the contract address.
		return contractInstance.getUtxos();
	}

	/**
	* Simulates contract outcome based on contract parameters, total satoshis in the contract and the redemption price.
	*
	* @param contractParameters    contract parameters including price boundaries and truncation information.
	* @param totalSats             total number of satoshis to simulate distribution of.
	* @param redeemPrice           price (units/BCH) to base the redemption simulation on.
	*
	* @returns the TXID of a successful transaction.
	*/
	async simulate(
		contractParameters: ContractParameters,
		totalSats: number,
		redeemPrice: number,
	): Promise<SimulationOutput>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'simulate() <=', arguments ]);

		// calculate the difference between the high and low truncation levels.
		const truncationDeltaSize = contractParameters.highLowDeltaTruncatedZeroes.length - contractParameters.lowTruncatedZeroes.length;

		// calculate the clamped price for the contract outcomes.
		const clampedPrice = Math.round(Math.min(contractParameters.priceRangeMax, Math.max(contractParameters.priceRangeMin, redeemPrice)));

		// Divide the untruncated hedge payour sats with the clamped price but..
		// store the remainder of the division to be used to retain accuracy.
		const hedgePayoutSatsHighTruncDiv = Math.floor(contractParameters.hedgeUnitsXSatsPerBchHighTrunc / clampedPrice);
		const hedgePayoutSatsHighTruncMod = Math.round(contractParameters.hedgeUnitsXSatsPerBchHighTrunc % clampedPrice);

		// partially untruncate the the hedge payout sats to get to the low truncation level..
		// .. then divide the lower bits by the price and add them to the low truncation hedge payout.
		const hedgePayoutSatsLowTruncDivBits = Math.floor(untruncScriptNum(hedgePayoutSatsHighTruncDiv, truncationDeltaSize));
		const hedgePayoutSatsLowTruncModBits = Math.floor(untruncScriptNum(hedgePayoutSatsHighTruncMod, truncationDeltaSize) / clampedPrice);
		const hedgePayoutSatsLowTrunc = hedgePayoutSatsLowTruncDivBits + hedgePayoutSatsLowTruncModBits;

		// calculate the longs payout as the available amount, minus the hedge payout value.
		const longPayoutSatsLowTrunc = contractParameters.payoutSatsLowTrunc - hedgePayoutSatsLowTrunc;

		// Untruncate both payout values and add the dust protection.
		const hedgePayoutSats = untruncScriptNum(hedgePayoutSatsLowTrunc, contractParameters.lowTruncatedZeroes.length) | DUST_LIMIT;
		const longPayoutSats = untruncScriptNum(longPayoutSatsLowTrunc, contractParameters.lowTruncatedZeroes.length) | DUST_LIMIT;

		// Calculate the total payout sats and consider the remainder to be miner fees.
		const payoutSats = hedgePayoutSats + longPayoutSats;
		const minerFeeSats = totalSats - payoutSats;

		const result = { hedgePayoutSats, longPayoutSats, payoutSats, minerFeeSats };

		// Write log entry for easier debugging.
		debug.action('Simulating contract outcomes.');

		// Output function result for easier collection of test data.
		debug.result([ 'simulate() =>', result ]);

		// Return the results of the calculation.
		return result;
	}

	/**
	* Builds a contract instance from contract parameters.
	*
	* @param contractParameters    contract parameters to build a contract instance from.
	*
	* @returns a contract instance.
	*/
	async build(contractHashes: ContractHashes): Promise<Instance>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'build() <=', arguments ]);

		// Write log entry for easier debugging.
		debug.action('Creating contract instance.');

		// Create an instance of the contract using the parameters above.
		const contract = this.contractFactory.new(
			contractHashes.mutualRedemptionDataHash,
			contractHashes.payoutDataHash,
		);

		// Output function result for easier collection of test data.
		debug.result([ 'build() =>', contract ]);

		// Pass back the contract to the caller.
		return contract;
	}

	/**
	* Creates a new contract.
	*
	* @param oraclePublicKeyBuffer   public key for the oracle that the contract trusts for price messages.
	* @param hedgePublicKeyBuffer    public key for the hedge party.
	* @param longPublicKeyBuffer     public key for the long party.
	* @param hedgeAmount             amount in units that the hedge party wants to protect against volatility.
	* @param contractPrice           starting price (units/BCH) of the contract.
	* @param contractBlockHeight     blockHeight at which the contract is considered to have been started at.
	* @param falloutModifier         minimum number of blocks from the starting height before the contract can be liquidated.
	* @param maturityModifier        exact number of blocks from the starting height that the contract should mature at.
	* @param valueLimitUp            multiplier for the contractPrice determining the upper liquidation price boundary.
	* @param valueLimitDown          multiplier for the contractPrice determining the lower liquidation price boundary.
	*
	* @returns the contract parameters, metadata and hashes.
	*/
	async create(
		oraclePublicKeyBuffer: Buffer,
		hedgePublicKeyBuffer: Buffer,
		longPublicKeyBuffer: Buffer,
		hedgeAmount: number,
		contractPrice: number,
		contractBlockHeight: number,
		falloutModifier: number,
		maturityModifier: number,
		valueLimitUp: number,
		valueLimitDown: number,
	): Promise<ContractData>
	{
		// Output function call arguments for easier collection of test data.
		// eslint-disable-next-line prefer-rest-params
		debug.params([ 'create() <=', arguments ]);

		// Store the public keys.
		const oraclePublicKey = oraclePublicKeyBuffer.toString('hex');
		const hedgePublicKey = hedgePublicKeyBuffer.toString('hex');
		const longPublicKey = longPublicKeyBuffer.toString('hex');

		// Determine the contract's upper price boundary without going below the start price
		// The calculated max is reduced by 1 to ensure hedge and long do not experience slippage at liquidation
		const maxPriceFloor = Math.ceil(contractPrice);
		const maxPriceCeiling = Math.floor((valueLimitUp * contractPrice) - 1);
		const priceRangeMax = Math.max(maxPriceFloor, maxPriceCeiling);

		// Determine the contract's lower price boundary without going above the start price
		// The calculated min is increased by 1 to ensure hedge and long do not experience slippage at liquidation
		const minPriceCeiling = Math.floor(contractPrice);
		const minPriceFloor = Math.round((valueLimitDown * contractPrice) + 1);
		const priceRangeMin = Math.min(minPriceFloor, minPriceCeiling);

		// Calculate the contract runtime.
		// The fallout provides a grace period in which the contract cannot settle.
		// The maturity provides a deadline after which anyone can settle the parameters.
		const earliestLiquidationHeight = contractBlockHeight + falloutModifier;
		const maturityHeight = contractBlockHeight + maturityModifier;

		// Copy the public key from the oracles wallet.
		const oraclePubk = oraclePublicKeyBuffer;

		// Assign the hedge and long lockscripts.
		const hedgeLockScript = buildLockScriptP2PKH(hedgePublicKeyBuffer);
		const longLockScript = buildLockScriptP2PKH(longPublicKeyBuffer);

		// Assign the hedge and long mutual cancellation public keys.
		const hedgeMutualRedeemPubk = hedgePublicKeyBuffer;
		const longMutualRedeemPubk = longPublicKeyBuffer;

		// NOTE: The following calculations are done to overcome a limitation
		//       in the data structures used by bitcoin script.

		// Calculate the naive input sats.
		// Hedge: satoshis equal to the hedged value at the contract price.
		// Long: satoshis equal to difference between the hedges satoshi and the hedged value at the lowest contract price.
		const naiveHedgeInputSats = Math.round((hedgeAmount * SATS_PER_BCH) / contractPrice);
		const naiveLongInputSats = Math.ceil(((hedgeAmount * SATS_PER_BCH) / priceRangeMin) - naiveHedgeInputSats);

		// Calculate the sum of the naive input sats.
		const naiveTotalInputSats = naiveHedgeInputSats + naiveLongInputSats;

		// ???
		const hedgeUnitsXSatsPerBch = Math.round(naiveHedgeInputSats * contractPrice);

		// Calculate the low and high truncation sizes.
		const truncationSizeHigh = calculateRequiredScriptNumTruncation(hedgeUnitsXSatsPerBch);
		const truncationSizeLow = calculateRequiredScriptNumTruncation(naiveTotalInputSats);

		// Calculate the diffrence between the low and high truncation sizes.
		const truncationSizeDelta = Math.abs(truncationSizeHigh - truncationSizeLow);

		// Create buffers with zeroes matching the low and deltra truncation sizes.
		const lowTruncatedZeroes = Buffer.alloc(truncationSizeLow);
		const highLowDeltaTruncatedZeroes = Buffer.alloc(truncationSizeDelta);

		// Truncate the hedge and payout values by the respective truncation sizes.
		const hedgeUnitsXSatsPerBchHighTrunc = truncScriptNum(hedgeUnitsXSatsPerBch, truncationSizeHigh);
		const payoutSatsLowTrunc = truncScriptNum(naiveTotalInputSats, truncationSizeLow);

		// ...
		const totalInputSats = untruncScriptNum(payoutSatsLowTrunc, truncationSizeLow);
		const hedgeInputSats = naiveHedgeInputSats;
		const longInputSats = totalInputSats - hedgeInputSats;

		// Store the total dust protection cost.
		const dustCost = 2 * DUST_LIMIT;

		// Store the miner transaction cost.
		const minerCost = MIN_TRANSACTION_FEE;

		// Calculate the long amount.
		const longAmount = ((longInputSats / SATS_PER_BCH) * contractPrice);

		// Create buffers for integer parameters for consistent assembly and hashing.
		const payoutSatsLowTruncBuffer =             bitbox.Script.encodeNumber(payoutSatsLowTrunc);
		const hedgeUnitsXSatsPerBchHighTruncBuffer = bitbox.Script.encodeNumber(hedgeUnitsXSatsPerBchHighTrunc);
		const priceRangeMinBuffer =                  bitbox.Script.encodeNumber(priceRangeMin);
		const priceRangeMaxBuffer =                  bitbox.Script.encodeNumber(priceRangeMax);
		const earliestLiquidationHeightBuffer =      bitbox.Script.encodeNumber(earliestLiquidationHeight);
		const maturityHeightBuffer =                 bitbox.Script.encodeNumber(maturityHeight);

		// Assemble the mutual redemption data.
		const mutualRedemptionData =
		[
			hedgeMutualRedeemPubk,
			longMutualRedeemPubk,
		];

		// Assemble the payout data.
		const payoutData =
		[
			hedgeLockScript,
			longLockScript,
			oraclePubk,
			hedgeUnitsXSatsPerBchHighTruncBuffer,
			highLowDeltaTruncatedZeroes,
			payoutSatsLowTruncBuffer,
			lowTruncatedZeroes,
			priceRangeMinBuffer,
			priceRangeMaxBuffer,
			earliestLiquidationHeightBuffer,
			maturityHeightBuffer,
		];

		// Calculate the hashes required to create a contract instance.
		const mutualRedemptionDataHash = bitbox.Crypto.ripemd160(bitbox.Crypto.sha256(Buffer.concat(mutualRedemptionData)));
		const payoutDataHash = bitbox.Crypto.ripemd160(bitbox.Crypto.sha256(Buffer.concat(payoutData)));

		// Assemble the contract parameters.
		const contractParameters =
		{
			priceRangeMin,
			priceRangeMax,
			earliestLiquidationHeight,
			maturityHeight,
			oraclePubk,
			hedgeLockScript,
			longLockScript,
			hedgeMutualRedeemPubk,
			longMutualRedeemPubk,
			lowTruncatedZeroes,
			highLowDeltaTruncatedZeroes,
			hedgeUnitsXSatsPerBchHighTrunc,
			payoutSatsLowTrunc,
		};

		const contractMetadata =
		{
			oraclePublicKey,
			hedgePublicKey,
			longPublicKey,
			contractBlockHeight,
			maturityModifier,
			falloutModifier,
			valueLimitUp,
			valueLimitDown,
			contractPrice,
			hedgeAmount,
			longAmount,
			totalInputSats,
			hedgeInputSats,
			longInputSats,
			dustCost,
			minerCost,
		};

		const contractHashes = { mutualRedemptionDataHash, payoutDataHash };

		const contractData =
		{
			parameters: contractParameters,
			metadata: contractMetadata,
			hashes: contractHashes,
		};

		// Write log entry for easier debugging.
		debug.action('Preparing a new contract.');
		const calculations =
		{
			maxPriceFloor,
			maxPriceCeiling,
			minPriceCeiling,
			minPriceFloor,
			naiveHedgeInputSats,
			naiveLongInputSats,
			naiveTotalInputSats,
			hedgeUnitsXSatsPerBch,
			truncationSizeHigh,
			truncationSizeLow,
			truncationSizeDelta,
		};
		debug.object(calculations);

		// Output function result for easier collection of test data.
		debug.result([ 'create() =>', contractData ]);

		// Pass back the contract data to the caller.
		return contractData;
	}
};
