export const SATS_PER_BCH = 100000000;
export const SCRIPT_BIT_MAX = 32;
export const SCRIPT_INT_MAX = (2 ** (SCRIPT_BIT_MAX - 1)) - 1;
export const MIN_TRANSACTION_FEE = 1500;
export const MAX_TRUNC_BYTES = 4;
export const DUST_LIMIT = 546;
