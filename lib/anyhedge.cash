pragma cashscript ^0.4.0;

// AnyHedge contract allows Hedge and Long to enter into an agreement where Hedge reduces their exposure
// to price volatility, measured in UNITS/BCH, and Long increases their exposure to the same volatility.
// AnyHedge also has a safety feature where Hedge and Long can exit the contract at any time through mutual agreement.
contract AnyHedge(
    //        We store a hash of fixed parameters in the scriptCode to make room for more actual code.
    //        The contents of each hash are detailed in redemption function parameters.
    //        Each hash is as a hash160: ripemd160(sha256(data)).
    //        The contents of each hash are verified at redemption.
    bytes20 mutualRedemptionDataHash,  // 20 B
    bytes20 payoutDataHash             // 20 B
) {

    // Mutual redemption is a safety feature where Hedge and Long can agree to exit the contract at any time.
    // It can be useful for example in the case of a funding error.
    // Note: This controls all contract inputs, such as fees, even if Hedge and Long did not provide them.
    function mutualRedeem(
        //      ------------------------------------------------------------------------------------------------
        //      Fixed parameters (the content of the mutual redemption hash stored in the scriptCode)
        //      ------------------------------------------------------------------------------------------------
        //      Hedge and Long public keys. Required to verify the mutual redemption transaction signatures.
        pubkey  hedgeMutualRedeemPubk,  // 33 B
        pubkey  longMutualRedeemPubk,   // 33 B

        //      ------------------------------------------------------------------------------------------------
        //      This redemption transaction's parameters
        //      ------------------------------------------------------------------------------------------------
        //      Hedge and Long signatures of the mutual redemption transaction.
        sig     hedgeMutualRedeemSig,   // 65 B
        sig     longMutualRedeemSig     // 65 B
    ) {
        // Before trusting the fixed parameters, we must confirm that they match the hash in the scriptCode.
        bytes20 providedHash = hash160(hedgeMutualRedeemPubk + longMutualRedeemPubk);
        require(mutualRedemptionDataHash == providedHash);

        // Verify that both Hedge and Long agree to the details of this transaction.
        require(checkSig(hedgeMutualRedeemSig, hedgeMutualRedeemPubk));
        require(checkSig(longMutualRedeemSig, longMutualRedeemPubk));
    }

    // Payout in Liquidation or Maturity conditions
    function payout(
        //        ------------------------------------------------------------------------------------------------
        //        Fixed parameters (the content of the payout hash stored in the scriptCode)
        //        ------------------------------------------------------------------------------------------------
        //        Arbitrary output lock scripts for Hedge and Long.
        //        AnyHedge can payout to p2pkh, p2sh or any valid output.
        //        E.g. p2pkh: pushLockScript + (opDup + opHash160 + pushHash + longPKH + opEqualVerify + opCheckSig)
        //        E.g. p2sh: pushLockScript + (opHash160 + pushHash + hedgeScriptHash + opEqual)
        //        An invalid lock script will make the contract un-redeemable so both must be validated carefully.
        bytes     hedgeLockScript,                  // 26 B for p2pkh, depends on script type
        bytes     longLockScript,                   // 26 B for p2pkh, depends on script type

        //        Oracle
        pubkey    oraclePubk,                       // 33 B, verifies message from oracle

        //        Money
        //        Note: All int types below must be minimally encoded both as arguments and in preimage construction.
        int       hedgeUnitsXSatsPerBch_HighTrunc,  // 1~4 B, truncated hedge payout in Units, scaled by 1e8(sats/BCH)
        bytes     highLowDeltaTruncatedZeroes,      // 0~3 B, used to scale numbers from highTrunc to lowTrunc
        int       payoutSats_LowTrunc,              // 1~4 B, truncated total payout sats, miner fee not included
        bytes     lowTruncatedZeroes,               // 0~4 B, used to scale numbers from lowTrunc to full size
        int       priceRangeMin,                    // 1~4 B, clamps price data to ensure valid payouts
        int       priceRangeMax,                    // 1~4 B, clamps price data to ensure valid payouts

        //        Time
        int       earliestLiquidationHeight,        // 3~4 B, earliest redemption height under liquidation conditions
        int       maturityHeight,                   // 3~4 B, required redemption height under maturity conditions

        //        ------------------------------------------------------------------------------------------------
        //        Redemption parameters
        //        ------------------------------------------------------------------------------------------------
        //        Transaction verification
        sig       preimageSig,                 // 65 B, verifies that provided preimage data matches the transaction
        pubkey    preimagePubk,                // 33 B, anyone can provide a key/signature to redeem this contract

        //        Oracle Data
        bytes50   oracleMsg,                   // 50 B, price(4) + blockHeight(4) + blockHash(32) + blockSequence(2) + oracleSequence(4) + oracleTimestamp (4)
        datasig   oracleSig                    // 64 B, signature of oracle message, verified with oracle's pubkey
    ) {
        // Before trusting the fixed parameters, we must confirm that they match the hash in the scriptCode.
        require(payoutDataHash == hash160(
            // Lock scripts
            hedgeLockScript +
            longLockScript +
            // Oracle
            oraclePubk +
            // Money
            bytes(hedgeUnitsXSatsPerBch_HighTrunc) +
            highLowDeltaTruncatedZeroes +
            bytes(payoutSats_LowTrunc) +
            lowTruncatedZeroes +
            bytes(priceRangeMin) +
            bytes(priceRangeMax) +
            // Time
            bytes(earliestLiquidationHeight) +
            bytes(maturityHeight)
        ));

        // Payout must happen through Liquidation or Maturity.
        // In both cases, we need to authenticate the oracle message.
        require(checkDataSig(oracleSig, oracleMsg, oraclePubk));

        // Covenant verification is automatically handled by CashScript.
        // By including this signature check, we gain access to all `tx.<...>` preimage fields.
        require(checkSig(preimageSig, preimagePubk));

        // Decode the parts of the oracle message that we need.
        bytes oracleTail1 = oracleMsg.split(4)[1];
        bytes oracleTail2 = oracleTail1.split(36)[1];

        int oracleBlockSequence = int(oracleTail2.split(2)[0]);
        int oraclePrice = int(oracleMsg.split(4)[0]);
        int oracleHeight = int(oracleTail1.split(4)[0]);

        // Fail if the oracle price is out of specification. Specifically:
        // 1. Fail if the price is negative.
        // 2. Fail if the price is zero.
        require(oraclePrice > 0);

        // Clamp the price within the allowed price range so that redemption will always be accurate and valid.
        int clampedPrice = max(min(oraclePrice, priceRangeMax), priceRangeMin);

        // Validate oracle timing and price
        // Here we add the complexity of using nLockTime so that we can get a protocol-level
        // guarantee about the height that the oracle claims. The guarantee that we want is that any claimed
        // oracle height is for an existing block, i.e. future oracle messages cannot be used to redeem a contract.
        //
        // This is a visualization of the oracle timing with each time point representing an oracle block height.
        // L: Earliest Liquidation block
        // M: Maturity block
        //             L          M
        //   <---------|----------|--------->
        // A <---------○                      (Redemption should fail)
        // B           ●----------○           (Liquidation is allowed only if oracle price is out of bounds)
        // C                      ●           (Maturity is allowed only for first price message in block M)
        // D                      ○---------> (Redemption should fail)
        //
        // Verify that oracle height is a valid block value.
        // Note that if oracleHeight is negative, that will be caught and failed by the checkLockTime below.
        // Note that the test for < 499999999 could be <= 499999999, but contract correctness verification was done
        // with the value 499999999 so it is left as is.
        require(oracleHeight < 499999999);

        // Verify at the protocol level that oracle height is an existing block up to and including tip.
        // This reduces the trust in the oracle because the contract can ignore future-dated oracle messages.
        // This also means that in order to redeem a contract at the earliest
        // block possible, user must submit the redemption transaction with (nLockTime = oracle).
        // Any (nLockTime >= oracle + x) will be redeemable for positive x, but the user will have to
        // wait for x blocks after the earliest redemption block.
        require(tx.time >= oracleHeight);

        // As in diagram above - if oracle height is before Liquidation or after Maturity --> Fail
        require(within(oracleHeight, earliestLiquidationHeight, maturityHeight + 1));

        // Because of above verifications of the reported oracle height, it can only be either:
        if (oracleHeight < maturityHeight) {
            // 1. During liquidation period.
            //    In this case, we must do an additional verification that the original oracle price is out of bounds.
            require(clampedPrice != oraclePrice);
        } else {
            // 2. Exactly at maturity height.
            //    In this case, the original oracle price can be in or out of bounds. Any clamped price is acceptable.
            //    However, we require the first message of the block, which is index 1 by specification
            require(oracleBlockSequence == 1);
        }

        // We survived the oracle validation gauntlet so redeem the contract with the clamped price.
        // Note: All calculation intermediate steps have been compressed into final results to reduce contract size.
        //       The names used in the documentation are labeled in the final results for reference.

        // Calculate hedge payout sats first.
        // Normally the mod (remainder) part of a script division is useless because it is less than zero.
        // However since we are un-truncating numbers (multiplying by a power of 2), we can do a math trick to
        // un-truncate and divide the mod part at the lower truncation level.
        // To begin, for our positive numbers:
        //     value/price =   (value DIV price)     + (value MOD price)      / price
        //                    ^hedgeDivHighTrunc^     ^hedgeModHighTrunc^
        //                                            ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ this is normally zero and useless
        // But we need to un-truncate before doing payout calculations and we get:
        //     value/price = hedgeDivHighTrunc *2^delta  + (hedgeModHighTrunc            /price) *2^delta
        // Or rearranging:
        //     value/price = hedgeDivHighTrunc *2^delta  + (hedgeModHighTrunc *2^delta)  /price
        //                   ^hedgeDivLowTrunc^^^^^^^^^     ^hedgeModLowTrunc^^^^^^^^^
        // Note: We know (value/price < payout) so un-truncated div and mod fit into a 4-byte script int
        //       because payout does. Therefore we interpret the un-truncated numbers as 4-bytes to handle any case.
        // Note: In the case highLowDeltaTruncatedZeroes is length 4, this would fail. However, due to the same
        //       guarantee above, that cannot happen with properly set price since it implies (value/price > payout).
        // Now we have a number that can be big enough to divide by the price so we repeat the div mod pattern:
        //     value/price = hedgeDivLowTrunc            + (hedgeModLowTrunc DIV price) + (hedgeModLowTrunc MOD price) /price
        //                                            ^additional precision^^   ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ again, zero
        // Note: We could do the hedgeDivLowTrunc + (hedgeModLowTrunc DIV price) addition with a bitwise-OR since
        //       high and low bits are separate but simple addition uses less opcodes.
        // Note: Un-truncation method
        //     Add any needed zeroes to the front (least significant bits), effectively multiplying by 2^size(zeroes)
        //     Total size is: size(zeroes) + number expanded to (targetSize-size(zeroes)) = targetSize
        //     e.g. zeroes = null (no zeroes); targetSize = 4
        //          total size = 0 + (4-0) = 4 bytes
        //     e.g. zeroes = 0x0000 (2 bytes of zeroes); targetSize = 8
        //          total size = 2 + (8-2) = 8 bytes
        //     Normally we need to minimally encode numbers, but here we can optimize by assuming 4 since the
        //     numbers are being converted in the end to minimal encoding by bin2num.
        int hedgeDivHighTrunc = hedgeUnitsXSatsPerBch_HighTrunc / clampedPrice;
        int hedgeDivLowTrunc  = int(highLowDeltaTruncatedZeroes + bytes4(hedgeDivHighTrunc));
        int hedgeModHighTrunc = hedgeUnitsXSatsPerBch_HighTrunc % clampedPrice;
        int hedgeModLowTrunc  = int(highLowDeltaTruncatedZeroes + bytes4(hedgeModHighTrunc));

        int hedgeSats_LowTrunc = hedgeDivLowTrunc + (hedgeModLowTrunc / clampedPrice);
        int longSats_LowTrunc  = payoutSats_LowTrunc - hedgeSats_LowTrunc;

        // Now we have a maximum precision (not perfect) hedge value we can use to calculate long payout.
        // We un-truncate from LowTrunc to full 8-byte numbers
        // Note: Dust safety
        //       If the price movement is very large, the final hedge or long sats could be less than the dust limit.
        //       Therefore We bitwise-OR the final output values with the dust amount to ensure that outputs are
        //       always viable. The dust amount is currently 546, hex 222, in little-endian 2202000.
        //       This requires adding at least 2x dust sats to the funding that the contract is unaware of.
        //       In almost all cases, the 2x dust will be split between adding to payouts and adding to the miner fee.
        // Note: Alternative dust methods
        //       There are other ways to ensure dust but they are more complicated, less robust and have
        //       a larger impact on the economics of the contract.
        bytes8 DUST = bytes8(546);
        bytes padding = lowTruncatedZeroes;
        int lowTruncSize = 8 - padding.length;

        // Calculate the hedge amount and pad it to 8 bytes
        // and apply dust safety by doing a bitwise OR with the DUST value
        bytes8 hedgeAmount = bytes8(padding + bytes(hedgeSats_LowTrunc, lowTruncSize)) | DUST;
        // Construct hedge output
        bytes hedgeOutput = hedgeAmount + hedgeLockScript;

        // Calculate the long amount and pad it to 8 bytes
        // and apply dust safety by doing a bitwise OR with the DUST value
        bytes8 longAmount = bytes8(padding + bytes(longSats_LowTrunc, lowTruncSize)) | DUST;
        // Construct long output
        bytes longOutput = longAmount + longLockScript;

        // Check that the hedge output + long output match this transaction's outputs
        require(tx.hashOutputs == hash256(hedgeOutput + longOutput));
    }
}
